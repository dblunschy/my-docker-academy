#!/bin/bash

echo "remove eth1-4"
rm -f /etc/sysconfig/network-scripts/ifcfg-eth1
rm -f /etc/sysconfig/network-scripts/ifcfg-eth2
rm -f /etc/sysconfig/network-scripts/ifcfg-eth3
rm -f /etc/sysconfig/network-scripts/ifcfg-eth4

echo "clean yum caches"
yum -y clean all

echo "(re)set vagrant initial ssh key"
mkdir -pm 700 /home/vagrant/.ssh
wget --no-check-certificate 'https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub' -O /home/vagrant/.ssh/authorized_keys
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh

echo "cleanup dirs."
rm -rf /home/vagrant/sync
rm -rf /home/vagrant/.ansible
rm -rf /tmp/*

echo "cleanup bash history, works only if this script is sourced."
# flush history to file
history -w
# clean history
history -c