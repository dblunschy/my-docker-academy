# Solutions

## Task 1
* mount the folder with busybox wihtout z ( no permission to write)
```
$ docker run --rm -it -v /root/demofolder:/tmp busybox /bin/sh
```

* mount the folder with jenkins with z ( permission to write)
```
$ docker run --rm -it -v /root/demofolder:/tmp:z jenkins /bin/bash
```


## Task 2
Goal is a postgres db container where the data is decoupled

### Task 2.1
* Create the data volume container
```
$ docker create -v /var/lib/postgresql/data -v /tmp --name academy-db-data --entrypoint /bin/true postgres
```

* Copy the init script to the data container
```
$ docker cp /vagrant/docker-academy/labs/volume-lab/sqlscripts/init.sql academy-db-data:/tmp
```

### Task 2.2
* Start the academy-db container
```
$ docker run -d -e POSTGRES_DB=academy -e POSTGRES_USER=academy -e POSTGRES_PASSWORD=academy --volumes-from academy-db-data --name=academy-db postgres
```

* Connect to the running postgres container
```
$ docker exec -it academy-db /bin/bash
root@0e0d8f4a4f12:/# psql -d academy -U academy -a -f /tmp/init.sql
```

### Task 2.3
* recreate academy-db container and mount academy-db-data and a new volume where the socket is
```
$ docker run -d -v /var/run/postgresql/ --volumes-from academy-db-data --name=academy-db postgres
``` 
* run a new postgres container and mount the academy-db server with volumes-from
```
$ docker run --volumes-from academy-db -it --rm -e PGPASSWORD=academy --entrypoint "" postgres sh -c 'psql -d academy -U academy -a'
```

### Task 3

* connect to the pgdb-server
```
 docker exec -it academy-db /bin/bash
``` 

* execute the following command
```
pg_dump -d academy -U academy > /var/lib/postgresql/data/pgdump.sql
```

* mount the academy-db-data on a busybox, gzip the data and move the to the host mounted directory. 
```
docker run -v $PWD:/tmp:Z --volumes-from academy-db-data -it busybox sh -c 'gzip /var/lib/postgresql/data/pgdump.sql && mv /var/lib/postgresql/data/pgdump.sql.gz /tmp'
```
