

## Generating Certs

### CA

openssl genrsa -aes256 -out ca-key.pem 4096
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem

ca-key.pem pw -> academy

### Server

openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN=10.0.0.100" -sha256 -new -key server-key.pem -out server.csr

echo subjectAltName = IP:192.168.33.100,IP:10.0.0.100,IP:127.0.0.1 > extfile.cnf

openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -extfile extfile.cnf

### Client

openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr

echo extendedKeyUsage = clientAuth > extfile_clientAuth.cnf

openssl x509 -req -days 730 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out cert.pem -extfile extfile_clientAuth.cnf

rm -v client.csr server.csr

chmod -v 0400 ca-key.pem key.pem server-key.pem


### Daemon

docker daemon --tlsverify --tlscacert=/etc/docker/certs.d/ca.pem --tlscert=/etc/docker/certs.d/server-cert.pem --tlskey=/etc/docker/certs.d/server-key.pem -H=0.0.0.0:2376

### Test

docker --tlsverify --tlscacert=ca.pem --tlscert=cert.pem --tlskey=key.pem -H 127.0.0.1:2376  version

## Conversion

### pem to pkcs12
 openssl pkcs12 -export -in cert.pem -inkey key.pem -out clientcert.p12


